package org.bitbucket.es4gwt.shared.elastic.query;

import javax.annotation.concurrent.Immutable;

/**
 * @author Mikael Couzic
 */
@Immutable
class MatchAll implements ElasticQuery {

	MatchAll() {
	}

	@Override
	public String toRequestString() {
		return "{\"match_all\":{}}";
	}

	@Override
	public String toString() {
		return toRequestString();
	}

}
