package org.bitbucket.es4gwt.shared.elastic.filter;

import java.util.Collection;

import javax.annotation.concurrent.Immutable;

/**
 * Smart "and" filter. If it is passed only one filter, it will behave as if it was that filter
 * 
 * @author Mikael Couzic
 */
@Immutable
class And extends BooleanOperator {

	And(Collection<ElasticFilter> filters) {
		super(filters);
	}

	@Override
	String getOperatorName() {
		return "and";
	}

}
