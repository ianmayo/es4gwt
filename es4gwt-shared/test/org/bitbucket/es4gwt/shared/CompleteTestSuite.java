package org.bitbucket.es4gwt.shared;

import org.bitbucket.es4gwt.shared.elastic.filter.FilterBuilderTest;
import org.bitbucket.es4gwt.shared.elastic.query.QueryBuilderTest;
import org.bitbucket.es4gwt.shared.spec.SpecTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FilterBuilderTest.class, QueryBuilderTest.class, SpecTestSuite.class })
public class CompleteTestSuite {

}
