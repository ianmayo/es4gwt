package org.bitbucket.es4gwt.shared.spec;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.bitbucket.es4gwt.shared.Facet.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bitbucket.es4gwt.shared.spec.FilterMode;
import org.bitbucket.es4gwt.shared.spec.SearchDate;
import org.bitbucket.es4gwt.shared.spec.SearchRequest;
import org.junit.Test;

public class SearchRequestTest {

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	SearchRequest spec = new SearchRequest();
	SearchRequest other = new SearchRequest();

	@Test
	public void emptyConfigs_equal() {
		assertThat(spec).isEqualTo(other);
		assertThat(spec.hashCode()).isEqualTo(other.hashCode());
	}

	@Test
	public void emptyConfig_otherWithText_notEqual() {
		other.fullTextSearch("some text", SEARCH_PARAMS);

		assertThat(spec).isNotEqualTo(other);
	}

	@Test
	public void emptyConfig_otherWithFacetFilter_notEqual() {
		other.withFacetFilter(PLATFORM_TYPE, "FISHER");

		assertThat(spec).isNotEqualTo(other);
	}

	@Test
	public void emptyConfig_otherWithDataFilterMode_notEqual() {
		other.withFilterMode(DATA_TYPE, FilterMode.ALL_OF);

		assertThat(spec).isNotEqualTo(other);
	}

	@Test
	public void emptyConfig_otherWithAfterDate_notEqual() {
		Date date = new Date();
		String dateString = dateFormat.format(date);

		other.after(new SearchDate(date, dateString));

		assertThat(spec).isNotEqualTo(other);
	}

	@Test
	public void emptyConfig_otherWithBeforeDate_Equal() {
		Date date = new Date();
		String dateString = dateFormat.format(date);

		other.before(new SearchDate(date, dateString));

		assertThat(spec).isNotEqualTo(other);
	}

}
