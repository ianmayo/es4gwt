package org.bitbucket.es4gwt.shared.spec;

import static org.bitbucket.es4gwt.shared.Facet.PLATFORM_TYPE;
import static org.fest.assertions.api.Assertions.assertThat;

import org.bitbucket.es4gwt.shared.spec.FacetFilters;
import org.junit.Test;

public class FacetFiltersTest {

	FacetFilters spec = new FacetFilters();
	FacetFilters other = new FacetFilters();

	@Test
	public void emptyConfigs_equal() {
		assertThat(spec).isEqualTo(other);
	}

	@Test
	public void emptyConfig_nonEmptyOther_notEqual() {
		other.with(PLATFORM_TYPE, "FERRY");

		assertThat(spec).isNotEqualTo(other);
	}

	@Test
	public void nonEmptyButEqual_sameHashCode() {
		spec.with(PLATFORM_TYPE, "hashcode");
		other.with(PLATFORM_TYPE, "hashcode");

		assertThat(spec.hashCode()).isEqualTo(other.hashCode());
	}

}
