package org.bitbucket.es4gwt.shared.spec;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FacetFiltersTest.class, SearchDateTest.class, SearchRequestTest.class })
public class SpecTestSuite {

}
