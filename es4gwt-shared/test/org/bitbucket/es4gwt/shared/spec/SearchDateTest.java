package org.bitbucket.es4gwt.shared.spec;

import static org.fest.assertions.api.Assertions.assertThat;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bitbucket.es4gwt.shared.spec.SearchDate;
import org.junit.Test;

public class SearchDateTest {

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	Date now = new Date();
	String nowString = dateFormat.format(now);

	Date alsoNow = new Date();
	String alsoNowString = dateFormat.format(alsoNow);

	SearchDate date = new SearchDate(now, nowString);
	SearchDate other = new SearchDate(alsoNow, alsoNowString);

	@Test
	public void now_equal() {
		assertThat(date).isEqualTo(other);
		assertThat(date.hashCode()).isEqualTo(other.hashCode());
	}

	@Test
	@SuppressWarnings("deprecation")
	public void now_yesterday_notEqual() {
		Date yesterday = new Date();
		yesterday.setDate(new Date().getDate() - 1);
		String yesterdayString = dateFormat.format(yesterday);
		other = new SearchDate(yesterday, yesterdayString);

		assertThat(date).isNotEqualTo(other);
	}

	@Test
	@SuppressWarnings("deprecation")
	public void now_otherDifferentHour_equal() {
		int hours = new Date().getHours();
		if (hours > 0)
			hours--;
		else
			hours = 23;
		Date differentHour = new Date();
		differentHour.setHours(hours);
		String differentHourString = dateFormat.format(differentHour);
		other = new SearchDate(differentHour, differentHourString);

		assertThat(date).isEqualTo(other);
		assertThat(date.hashCode()).isEqualTo(other.hashCode());
	}
}
