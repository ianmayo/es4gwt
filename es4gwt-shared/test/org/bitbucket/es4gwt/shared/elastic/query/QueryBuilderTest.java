package org.bitbucket.es4gwt.shared.elastic.query;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.bitbucket.es4gwt.shared.Facet.*;

import org.bitbucket.es4gwt.shared.elastic.query.QueryBuilder;
import org.junit.Test;

public class QueryBuilderTest {

	private QueryBuilder builder = new QueryBuilder();

	@Test
	public void noFilter_matchAll() {
		assertThat(builder.buildQueryString()).isEqualTo("{\"match_all\":{}}");
	}

	@Test
	public void oneFilter_SingleValue() {
		// given
		builder.filter(PLATFORM_TYPE, "FISHER");

		// when
		String query = builder.buildQueryString();

		// then
		assertThat(query).isEqualTo("{\"filtered\":{\"query\":{\"match_all\":{}},\"filter\":{\"term\":{\"platform_type\":\"FISHER\"}}}}");
	}

	@Test
	public void oneFilter_TwoValues() {
		// given
		builder.filter(PLATFORM_TYPE, "FISHER");
		builder.filter(PLATFORM_TYPE, "FERRY");

		// when
		String query = builder.buildQueryString();

		// then
		assertThat(query).isEqualTo("{\"filtered\":{\"query\":{\"match_all\":{}},\"filter\":{\"terms\":{\"platform_type\":[\"FISHER\",\"FERRY\"]}}}}");
	}

	@Test
	public void twoFilters_OneValue() {
		// given
		builder.filter(PLATFORM_TYPE, "FERRY");
		builder.filter(SENSOR_TYPE, "RADAR");

		// when
		String query = builder.buildQueryString();

		// then
		assertThat(query).startsWith("{\"filtered\":{\"query\":{\"match_all\":{}},\"filter\":{\"and\":[{\"term\":{")
							.contains("{\"term\":{\"platform_type\":\"FERRY\"}}")
							.contains("{\"term\":{\"sensor_type\":\"RADAR\"}}")
							.endsWith("\"}}]}}}");
	}

	@Test
	public void twoFilters_twoValues() {
		// given
		builder.filter(PLATFORM_TYPE, "FISHER");
		builder.filter(PLATFORM_TYPE, "FERRY");
		builder.filter(SENSOR_TYPE, "RADAR");
		builder.filter(SENSOR_TYPE, "GPS");

		// when
		String query = builder.buildQueryString();

		// then
		assertThat(query).startsWith("{\"filtered\":{\"query\":{\"match_all\":{}},\"filter\":{\"and\":[{\"terms\":{")
							.contains("{\"terms\":{\"platform_type\":[\"FISHER\",\"FERRY\"]}}")
							.contains("{\"terms\":{\"sensor_type\":[\"RADAR\",\"GPS\"]}}")
							.endsWith("\"]}}]}}}");
	}
}
