package org.bitbucket.es4gwt.shared.elastic.filter;

import static org.bitbucket.es4gwt.shared.Facet.*;
import static org.fest.assertions.api.Assertions.*;

import org.bitbucket.es4gwt.shared.elastic.filter.FilterBuilder;
import org.bitbucket.es4gwt.shared.spec.FilterMode;
import org.junit.Test;

public class FilterBuilderTest {

	FilterBuilder builder = new FilterBuilder();

	@Test
	public void singleFacet_singleFilter_forQuery() {
		// given
		builder.withTerm(PLATFORM_TYPE, "FERRY");

		// when
		String filter = builder.buildFilterString();

		// then
		assertThat(filter).isEqualTo("{\"term\":{\"platform_type\":\"FERRY\"}}");
	}

	@Test
	public void singleFacet_singleFilter_forSameFacet_fails() {
		// given
		builder.withTerm(PLATFORM_TYPE, "FERRY");

		// when
		boolean hasFilter = builder.hasFilterFor(PLATFORM_TYPE);

		// then
		assertThat(hasFilter).isFalse();

		try {
			builder.buildFilterFor(PLATFORM_TYPE);
			failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage()).containsIgnoringCase("empty set");
		}
	}

	@Test
	public void singleFacet_singleFilter_facetScope_forDifferentFacet() {
		// given
		builder.withTerm(PLATFORM_TYPE, "FERRY");

		// when
		String filter = builder.buildFilterStringFor(SENSOR_TYPE);

		// then
		assertThat(filter).isEqualTo("{\"term\":{\"platform_type\":\"FERRY\"}}");
	}

	@Test
	public void singleFacet_multipleFiltersPerFacet_forQuery() {
		// given
		builder.withTerm(PLATFORM_TYPE, "FISHER");
		builder.withTerm(PLATFORM_TYPE, "FERRY");

		// when
		String filter = builder.buildFilterString();

		// then
		assertThat(filter).isEqualTo("{\"terms\":{\"platform_type\":[\"FISHER\",\"FERRY\"]}}");
	}

	@Test
	public void multipleFacets_singleFilterPerFacet_forQuery() {
		// given
		builder.withTerm(PLATFORM_TYPE, "FISHER");
		builder.withTerm(SENSOR_TYPE, "RADAR");

		// when
		String filter = builder.buildFilterString();

		// then
		assertThat(filter).startsWith("{\"and\":[{\"term\":{\"")
							.contains("{\"term\":{\"platform_type\":\"FISHER\"}}")
							.contains("{\"term\":{\"sensor_type\":\"RADAR\"}}")
							.endsWith("\"}}]}");
	}

	@Test
	public void multipleFacets_singleFilterPerFacet_forFacet() {
		// given
		builder.withTerm(PLATFORM_TYPE, "FISHER");
		builder.withTerm(SENSOR_TYPE, "RADAR");

		// when
		String filter = builder.buildFilterStringFor(PLATFORM_TYPE);

		// then
		assertThat(filter).isEqualTo("{\"term\":{\"sensor_type\":\"RADAR\"}}");
	}

	@Test
	public void multipleFacets_multipleFiltersPerFacet_forQuery() {
		// given
		builder.withTerm(PLATFORM_TYPE, "FERRY");
		builder.withTerm(PLATFORM_TYPE, "FISHER");
		builder.withTerm(SENSOR_TYPE, "GPS");
		builder.withTerm(SENSOR_TYPE, "RADAR");

		// when
		String filter = builder.buildFilterString();

		// then
		assertThat(filter).startsWith("{\"and\":[{\"terms\":{\"")
							.contains("{\"terms\":{\"platform_type\":[\"FERRY\",\"FISHER\"]}}")
							.contains("{\"terms\":{\"sensor_type\":[\"GPS\",\"RADAR\"]}}")
							.endsWith("\"]}}]}");
	}

	@Test
	public void withFilterMode_FOR_ALL() {
		// given
		builder.withTerm(DATA_TYPE, "time");
		builder.withTerm(DATA_TYPE, "depth");
		builder.filterMode(DATA_TYPE, FilterMode.ALL_OF);

		// when
		String filter = builder.buildFilterString();

		// then
		assertThat(filter).isEqualTo("{\"terms\":{\"data_type\":[\"time\",\"depth\"],\"execution\":\"and\"}}");
	}
}
