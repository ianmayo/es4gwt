package org.bitbucket.es4gwt.client.result;

public interface ResultTerm {

	String getTerm();

	int getCount();
}
