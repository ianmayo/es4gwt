package org.bitbucket.es4gwt.client.result;

import com.google.gwt.core.client.JavaScriptObject;

class SearchHitJSON extends JavaScriptObject implements SearchHit {

	protected SearchHitJSON() {
	}

	@Override
	public final native String getId() /*-{
		return this._id;
	}-*/;

	@Override
	public final native void setId(String id) /*-{
		this._id = id;
	}-*/;

	@Override
	public final native String getName() /*-{
		return this.fields["metadata.name"];
	}-*/;

	@Override
	public final native String getPlatform() /*-{
		return this.fields["metadata.platform"];
	}-*/;

	@Override
	public final native String getPlatformType() /*-{
		return this.fields["metadata.platform_type"];
	}-*/;

	@Override
	public final native String getSensor() /*-{
		return this.fields["metadata.sensor"];
	}-*/;

	@Override
	public final native String getSensorType() /*-{
		return this.fields["metadata.sensor_type"];
	}-*/;

	@Override
	public final native String getTrial() /*-{
		return this.fields["metadata.trial"];
	}-*/;

	@Override
	public final native String getType() /*-{
		return this.fields["metadata.type"];
	}-*/;

	@Override
	public final native String getStart() /*-{
		return this.fields["metadata.time_bounds.start"];
	}-*/;

	@Override
	public final native String getEnd() /*-{
		return this.fields["metadata.time_bounds.end"];
	}-*/;

	// @SuppressWarnings("deprecation")
	// @Override
	// public final Date getStartDate() {
	// String[] split = getStart().split("T")[0].split("-");
	// if (split.length != 3)
	// return null;
	// Date date = new Date();
	// date.setYear(Integer.valueOf(split[0]));
	// date.setMonth(Integer.valueOf(split[1]));
	// date.setDate(Integer.valueOf(split[2]));
	// return date;
	// }

	// @SuppressWarnings("deprecation")
	// @Override
	// public final Date getEndDate() {
	// String[] split = getEnd().split("T")[0].split("-");
	// if (split.length != 3)
	// return null;
	// Date date = new Date();
	// date.setYear(Integer.valueOf(split[0]));
	// date.setMonth(Integer.valueOf(split[1]));
	// date.setDate(Integer.valueOf(split[2]));
	// return date;
	// }

}
