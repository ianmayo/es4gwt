package org.bitbucket.es4gwt.client.result;

import com.google.gwt.core.client.JavaScriptObject;

public class ResultTermJSON extends JavaScriptObject implements ResultTerm {

	protected ResultTermJSON() {
	}

	public final native String getTerm() /*-{
		return this.term;
	}-*/;

	public final native int getCount() /*-{
		return this.count;
	}-*/;

}
