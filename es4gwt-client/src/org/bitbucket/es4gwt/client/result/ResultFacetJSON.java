package org.bitbucket.es4gwt.client.result;

import java.util.Iterator;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Dataset object
 * 
 * { "_type" : "terms", "missing" : 0, "total" : 5, "other" : 0, "terms" : [ { "term" : "roger", "count" : 3 }, { "term"
 * : "tomato", "count" : 1 }, { "term" : "jolly", "count" : 1 } ] }
 * 
 * @author Yuri
 * 
 */
class ResultFacetJSON extends JavaScriptObject implements ResultFacet {

	protected ResultFacetJSON() {
	}

	@Override
	public final native int getNumTerms() /*-{
		if (this.terms == null)
			return 0;
		else
			return this.terms.length;
	}-*/;

	@Override
	public final native ResultTermJSON getTerm(int index) /*-{
		return this.terms[index];
	}-*/;

	@Override
	public final Iterator<ResultTerm> iterator() {
		return new ResultFacetIterator();
	}

	private final class ResultFacetIterator implements Iterator<ResultTerm> {

		private int current = 0;

		@Override
		public boolean hasNext() {
			return current < getNumTerms();
		}

		@Override
		public ResultTerm next() {
			if (!hasNext())
				throw new IndexOutOfBoundsException();
			return getTerm(current++);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}
}
