package org.bitbucket.es4gwt.client.result;

import java.util.Iterator;

public interface ResultFacet {

	int getNumTerms();

	ResultTerm getTerm(int index);

	Iterator<ResultTerm> iterator();
}
