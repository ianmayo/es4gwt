package org.bitbucket.es4gwt.client.result;

public interface Selectable {

	void setSelected(boolean purchased);

	boolean isSelected();
}
