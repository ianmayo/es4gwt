package org.bitbucket.es4gwt.client.result;

import java.util.List;

import org.bitbucket.es4gwt.shared.elastic.ElasticFacet;

public interface SearchResult {

	int getElapsedMillis();

	boolean getTimeout();

	int getHitCount();

	SearchHit getHit(int i);

	ResultFacet getFacet(String facetName);

	ResultFacet getFacet(ElasticFacet facet);

	List<ResultTableElement> list();
}
