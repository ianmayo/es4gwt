package org.bitbucket.es4gwt.client.result;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Set;

import javax.annotation.concurrent.Immutable;

import com.google.common.collect.Sets;

@Immutable
public class SearchResultDiff {

	private final SearchResult oldResult, newResult;

	private final Set<ResultTableElement> added, removed;

	public SearchResultDiff(SearchResult oldResult, SearchResult newResult) {
		checkNotNull(oldResult);
		checkNotNull(newResult);

		this.oldResult = oldResult;
		this.newResult = newResult;

		List<ResultTableElement> oldHits = oldResult.list();
		List<ResultTableElement> newHits = newResult.list();

		added = Sets.newHashSet(newHits);
		added.removeAll(oldHits);

		removed = Sets.newHashSet(oldHits);
		removed.removeAll(newHits);

	}

	public SearchResult getOldResult() {
		return oldResult;
	}

	public SearchResult getNewResult() {
		return newResult;
	}

	public boolean hasChanges() {
		return hasAdded() || hasRemoved();
	}

	public boolean hasAdded() {
		return added.size() != 0;
	}

	public boolean hasRemoved() {
		return removed.size() != 0;
	}

	public Set<ResultTableElement> added() {
		return added;
	}

	public Set<ResultTableElement> removed() {
		return removed;
	}

}
