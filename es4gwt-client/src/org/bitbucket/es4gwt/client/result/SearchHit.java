package org.bitbucket.es4gwt.client.result;

// TODO Replace String types by more relevant types (Enum types, Date...)
public interface SearchHit {

	String getId();

	void setId(String id);

	String getName();

	String getPlatform();

	String getPlatformType();

	String getSensor();

	String getSensorType();

	String getTrial();

	String getType();

	String getStart();

	String getEnd();

}
