package org.bitbucket.es4gwt.client.result;

public interface ResultTableElement extends SearchHit, Selectable {

	void setAdded(boolean added);

	boolean isAdded();

	void setRemoved(boolean removed);

	boolean isRemoved();

}
