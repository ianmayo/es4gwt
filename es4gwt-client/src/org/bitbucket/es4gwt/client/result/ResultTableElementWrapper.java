package org.bitbucket.es4gwt.client.result;

import static com.google.common.base.Preconditions.*;

class ResultTableElementWrapper implements ResultTableElement {

	private final SearchHit wrapped;

	private boolean selected;
	private boolean added;
	private boolean removed;

	ResultTableElementWrapper(SearchHit searchHit) {
		checkNotNull(searchHit);
		wrapped = searchHit;
	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setAdded(boolean added) {
		this.added = added;
	}

	@Override
	public boolean isAdded() {
		return added;
	}

	@Override
	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	@Override
	public boolean isRemoved() {
		return removed;
	}

	@Override
	public int hashCode() {
		return wrapped.getId().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ResultTableElement))
			return false;
		return wrapped.getId().equals(((ResultTableElement) obj).getId());
	}

	// TODO : Delegate with Lombok

	@Override
	public String getId() {
		return wrapped.getId();
	}

	@Override
	public void setId(String id) {
		wrapped.setId(id);
	}

	@Override
	public String getName() {
		return wrapped.getName();
	}

	@Override
	public String getPlatform() {
		return wrapped.getPlatform();
	}

	@Override
	public String getPlatformType() {
		return wrapped.getPlatformType();
	}

	@Override
	public String getSensor() {
		return wrapped.getSensor();
	}

	@Override
	public String getSensorType() {
		return wrapped.getSensorType();
	}

	@Override
	public String getTrial() {
		return wrapped.getTrial();
	}

	@Override
	public String getType() {
		return wrapped.getType();
	}

	@Override
	public String getStart() {
		return wrapped.getStart();
	}

	@Override
	public String getEnd() {
		return wrapped.getEnd();
	}
}
