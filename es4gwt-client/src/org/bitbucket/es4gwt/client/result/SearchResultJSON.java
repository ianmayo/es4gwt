package org.bitbucket.es4gwt.client.result;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.es4gwt.shared.SearchConstants;
import org.bitbucket.es4gwt.shared.elastic.ElasticFacet;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Dataset object
 * 
 * { "took" : 6, "timed_out" : false, "_shards" : { "total" : 5, "successful" : 5, "failed" : 0 }, "hits" : { "total" :
 * 5, "max_score" : 2.1410832, "hits" : [ { "_index" : "gnd", "_type" : "datasets", "_id" :
 * "72b6efa2c581e7615bb765208312fd17_1", "_score" : 2.1410832, "_source" : {"sensor"
 * :"Garmin-G400","platform":"plat_a","sensor_type":"speed","data_type":
 * ["time","lon","lat"],"platform_type":"van","time_bounds":{"start": "2012-03-15T03:46:37+0000"
 * ,"end":"2012-03-15T16:35:58+0000"},"name":"Track 30" ,"type":"track","geo_bounds"
 * :{"tl":[-7.784170959924891,57.944529113991734],"br" :[-5.019334420312834,49.109100753386016]},"trial":"trial_d"} } ]
 * } }
 * 
 * @author Yuri
 * 
 */
public class SearchResultJSON extends JavaScriptObject implements SearchResult {

	protected SearchResultJSON() {
	}

	@Override
	public final native int getElapsedMillis() /*-{
		return this.took;
	}-*/;

	@Override
	public final native boolean getTimeout() /*-{
		return this.timed_out;
	}-*/;

	@Override
	public final native int getHitCount() /*-{
		return this.hits.total;
	}-*/;

	@Override
	public final native SearchHit getHit(int i) /*-{
		return this.hits.hits[i];
	}-*/;

	@Override
	public final native ResultFacet getFacet(String facetName) /*-{
		return this.facets[facetName];
	}-*/;

	@Override
	public final ResultFacet getFacet(ElasticFacet facet) {
		return getFacet(facet.toRequestString());
	}

	@Override
	public final List<ResultTableElement> list() {
		List<ResultTableElement> list = new ArrayList<ResultTableElement>();
		int returnedHitsCount = Math.min(getHitCount(), SearchConstants.MAX_RESULTS);
		for (int i = 0; i < returnedHitsCount; i++)
			list.add(new ResultTableElementWrapper(getHit(i)));
		return list;
	}

}
