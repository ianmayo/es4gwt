package org.bitbucket.es4gwt.client;

import java.util.List;

import org.bitbucket.es4gwt.client.result.ResultFacet;
import org.bitbucket.es4gwt.client.result.ResultTableElement;
import org.bitbucket.es4gwt.client.result.SearchHit;
import org.bitbucket.es4gwt.client.result.SearchResult;
import org.bitbucket.es4gwt.shared.elastic.ElasticFacet;

/**
 * Optimization class, might not be needed. Not used yet
 */
class ResultWrapperLazyList implements SearchResult {

	private final SearchResult wrapped;

	private List<ResultTableElement> lazyList;

	ResultWrapperLazyList(SearchResult toWrap) {
		this.wrapped = toWrap;
	}

	@Override
	public List<ResultTableElement> list() {
		if (lazyList == null)
			lazyList = wrapped.list();
		return lazyList;
	}

	// ///////////////////////////////////////

	@Override
	public int getElapsedMillis() {
		return wrapped.getElapsedMillis();
	}

	@Override
	public boolean getTimeout() {
		return wrapped.getTimeout();
	}

	@Override
	public SearchHit getHit(int i) {
		return wrapped.getHit(i);
	}

	@Override
	public int getHitCount() {
		return wrapped.getHitCount();
	}

	@Override
	public ResultFacet getFacet(String facetName) {
		return wrapped.getFacet(facetName);
	}

	@Override
	public ResultFacet getFacet(ElasticFacet facet) {
		return wrapped.getFacet(facet);
	}

}
