package config;

import org.junit.Before;
import org.mockito.MockitoAnnotations;

public abstract class TestWithMocks {

	@Before
	public void setupMocks() {
		MockitoAnnotations.initMocks(this);
	}

}
