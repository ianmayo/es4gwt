package org.bitbucket.es4gwt.client;

import org.bitbucket.es4gwt.client.result.SearchResultDiffTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SearchResultDiffTest.class })
public class TestSuite {

}
