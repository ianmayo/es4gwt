package org.bitbucket.es4gwt.client.result;

import static org.mockito.Mockito.*;
import static org.bitbucket.es4gwt.client.result.SearchResultMockFactory.newSearchResultMock;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.thirdparty.guava.common.collect.Sets;

public class SearchResultDiffMockBuilder {

	private final SearchResultDiff mock = mock(SearchResultDiff.class);

	private SearchResultDiffMockBuilder() {
	}

	public static SearchResultDiffMockBuilder diffBetween(SearchResult oldResult) {
		SearchResultDiffMockBuilder builder = new SearchResultDiffMockBuilder();
		when(builder.mock.getOldResult()).thenReturn(oldResult);
		return builder;
	}

	public static SearchResultDiffMockBuilder diffBetween(ResultTableElement... oldHits) {
		return diffBetween(newSearchResultMock(oldHits));
	}

	public static SearchResultDiffMockBuilder diffBetweenEmptyResult() {
		return diffBetween(newSearchResultMock(new ArrayList<ResultTableElement>()));
	}

	public SearchResultDiffMockBuilder and(SearchResult newResult) {
		when(mock.getNewResult()).thenReturn(newResult);
		return this;
	}

	public SearchResultDiffMockBuilder and(ResultTableElement... newHits) {
		return and(newSearchResultMock(newHits));
	}

	public SearchResultDiffMockBuilder added(ResultTableElement... addedHits) {
		when(mock.added()).thenReturn(Sets.newHashSet(addedHits));
		return this;
	}

	public SearchResultDiffMockBuilder removed(Set<ResultTableElement> removedHits) {
		when(mock.removed()).thenReturn(Sets.newHashSet(removedHits));
		return this;
	}

	public SearchResultDiffMockBuilder removed(ResultTableElement... removedHits) {
		removed(Sets.newHashSet(removedHits));
		return this;
	}

	public SearchResultDiffMockBuilder nothingRemoved() {
		removed(new HashSet<ResultTableElement>());
		return this;
	}

	public SearchResultDiff get() {
		return mock;
	}

}
