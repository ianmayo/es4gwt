package org.bitbucket.es4gwt.client.result;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;

import org.junit.Test;
import org.mockito.Mock;

import config.JukitoTest;

public class SearchResultDiffTest extends JukitoTest {

	@Mock SearchResult oldResult;
	@Mock SearchResult newResult;

	@Test
	public void equalResults_noChanges(SearchResult sameResult) {

		// when
		SearchResultDiff diff = new SearchResultDiff(oldResult, newResult);

		// then
		assertThat(diff.hasChanges()).isFalse();
	}

	@Test
	public void oldIsEmpty_newSingleHit_newHitAdded(ArrayList<ResultTableElement> list, ResultTableElement newHit) {
		// given
		given(newResult.list()).willReturn(list);
		list.add(newHit);
		given(newHit.getId()).willReturn("ID1");

		// when
		SearchResultDiff diff = new SearchResultDiff(oldResult, newResult);

		// then
		assertThat(diff.hasAdded()).isTrue();
		assertThat(diff.added()).containsOnly(newHit);
		assertThat(diff.hasRemoved()).isFalse();
		assertThat(diff.removed()).isEmpty();
	}

	@Test
	public void oldSingleHit_newIsEmpty_oldHitRemoved(ArrayList<ResultTableElement> list, ResultTableElement oldHit) {
		// given
		given(oldResult.list()).willReturn(list);
		list.add(oldHit);
		given(oldHit.getId()).willReturn("ID1");

		// when
		SearchResultDiff diff = new SearchResultDiff(oldResult, newResult);

		// then
		assertThat(diff.hasRemoved()).isTrue();
		assertThat(diff.removed()).containsOnly(oldHit);
		assertThat(diff.hasAdded()).isFalse();
		assertThat(diff.added()).isEmpty();
	}

}
