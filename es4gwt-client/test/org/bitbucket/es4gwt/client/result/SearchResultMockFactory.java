package org.bitbucket.es4gwt.client.result;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

public class SearchResultMockFactory {

	public static SearchResult newSearchResultMock(List<ResultTableElement> list) {
		SearchResult mock = mock(SearchResult.class);
		when(mock.list()).thenReturn(list);
		return mock;
	}

	public static SearchResult newSearchResultMock(ResultTableElement... elements) {
		return newSearchResultMock(Lists.newArrayList(elements));
	}

	public static SearchResult newSearchResultMock() {
		return newSearchResultMock(new ArrayList<ResultTableElement>());
	}

}
